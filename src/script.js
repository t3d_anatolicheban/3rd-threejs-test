import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'lil-gui'
import { AmbientLight, BoxGeometry, DirectionalLight, DirectionalLightHelper, Group, Mesh, MeshStandardMaterial, PlaneGeometry, SphereGeometry } from 'three'
import * as CANNON from 'cannon-es'
import { Vec3 } from 'cannon-es'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'

/**
 * Base
 */
// Debug
const gui = new dat.GUI()

//Loader
const gltfLoader = new GLTFLoader()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.set(9, 9, 9)
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))




//TEST

//Physics wo]rld
let world = new CANNON.World()
world.gravity.set(0, -9.82, 0)
world.allowSleep = true
world.broadphase = new CANNON.SAPBroadphase(world)

const defaultMaterial = new CANNON.Material('default')

const defaultContactMaterial = new CANNON.ContactMaterial(defaultMaterial, defaultMaterial, {
    friction: 1000,
    restitution: .2
})

world.addContactMaterial(defaultContactMaterial)
world.defaultContactMaterial = defaultContactMaterial

const floorShape = new CANNON.Plane()
const floorBody = new CANNON.Body({
    material: defaultMaterial
})
floorBody.mass = 0
floorBody.quaternion.setFromAxisAngle(new CANNON.Vec3(-1, 0, 0), Math.PI / 2)
floorBody.addShape(floorShape)
world.addBody(floorBody)

const sphereShape = new CANNON.Sphere(.5)
const sphereBody = new CANNON.Body({
    position: new CANNON.Vec3(0, 3, 5),
    shape: sphereShape,
    material: defaultMaterial,
    mass: 1
})
sphereBody.angularDamping = .2
world.addBody(sphereBody)

//THREEJS SCENE
const ambLight = new AmbientLight('#fff', .2)
scene.add(ambLight)
const dirLight = new DirectionalLight('#fff', 1)
dirLight.position.set(4, 5, 2)
scene.add(new DirectionalLightHelper(dirLight))
scene.add(dirLight)

const material = new MeshStandardMaterial({ color: 'pink' })

const floorGeometry = new PlaneGeometry(6, 15)
const floor = new Mesh(floorGeometry, material)
floor.material.color.set('#ffff00')
floor.rotation.x = -Math.PI / 2
scene.add(floor)

// const sphereGeometry = new SphereGeometry(.5)
// const sphere = new Mesh(sphereGeometry, material)
// sphere.position.set(0, 3, 5)
// scene.add(sphere)

let burger;

gltfLoader.load('/hamburger.glb', (gltf) => {
    burger = gltf.scene
    burger.scale.set(.13, .13, .13)
    scene.add(burger)
})

const cubeGeometry = new BoxGeometry(1, 1)
let tower;
let towerElements = []

const buildTower = (height) => {
    if (tower) {
        scene.remove(tower)
    }

    if (towerElements.length) {
        towerElements.forEach(el => {
            world.removeBody(el.body)
            scene.remove(el.mesh)
        })
        towerElements = []
    }

    tower = new Group()

    for (let x = 0; x < 3; x++) {
        for (let y = 0; y < height; y++) {
            for (let z = 0; z < 3; z++) {
                const cubeMaterial = material.clone()
                let cube = new Mesh(cubeGeometry, cubeMaterial)
                cube.material.color.setRGB(
                    (Math.random() - .5) * 1,
                    (Math.random() - .5) * 1,
                    .8 + (Math.random() - .5) * 1,
                )

                let cubeBody = new CANNON.Body({
                    mass: .3,
                    material: defaultMaterial,
                    shape: new CANNON.Box(new CANNON.Vec3(.5, .5, .5)),
                    position: new CANNON.Vec3(x - 1, y + .5, z - 5)
                })
                cube.position.copy(cubeBody.position)
                console.log(cubeBody.position);
                tower.add(cube)
                world.addBody(cubeBody)
                towerElements.push({ mesh: cube, body: cubeBody })
            }
        }
    }
    scene.add(tower)
}


const resetScene = (height) => {
    buildTower(height)
    sphereBody.position.set(0, 3, 5)
}

const shoot = () => {
    if (burger) {
        sphereBody.mass = 1
        sphereBody.applyImpulse(new Vec3(0, 10, -70))
    }
}

//GUI
const params = {
    shoot() {
        shoot()
    },
    reset() {
        resetScene(this.towerHeight)
    },
    buildTower() {
        buildTower(this.towerHeight)
    },
    towerHeight: 5
}

buildTower(params.towerHeight)

gui.add(params, 'shoot').name('Shoot')
gui.add(params, 'reset').name('Reset')
gui.add(params, 'towerHeight').min(3).max(7).step(1).name('Tower Height')
gui.add(params, 'buildTower').name('Build Tower')

/**
 * Animate
 */
const clock = new THREE.Clock()
let oldElapsedTime = 0;

const tick = () => {
    const elapsedTime = clock.getElapsedTime()
    const deltaTime = elapsedTime - oldElapsedTime;
    oldElapsedTime = elapsedTime

    // Update controls
    controls.update()
    // console.log(sphereBody.position);


    //Update physics
    world.step(1 / 60, deltaTime, 3)
    if (burger) {
        burger.position.copy(sphereBody.position)
        burger.quaternion.copy(sphereBody.quaternion)
    }

    towerElements.forEach(el => {
        el.mesh.position.copy(el.body.position)
        el.mesh.quaternion.copy(el.body.quaternion)
    })

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()